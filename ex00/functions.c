/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   functions.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/18 12:06:22 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/18 14:43:57 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/*
** ft_putchar: Print character to stdout
*/

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

/*
** ft_putstr: Print char array to stdout
*/

void	ft_putstr(char *str)
{
	while (*str != '\0')
	{
		ft_putchar(*str);
		str++;
	}
}

/*
** ft_ctoi: Convert char to int
** Accepts 1-9, dots are translated to 0
** Returns -1 when char is invalid
*/

int		ft_ctoi(char c)
{
	if (c == '.')
		return (0);
	if (c < '1' || c > '9')
		return (-1);
	return (c - '0');
}

/*
** ft_strlen: Calculate string size
*/

int		ft_strlen(char *str)
{
	int counter;

	counter = 0;
	while (str[counter] != '\0')
		counter++;
	return (counter);
}
