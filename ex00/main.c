/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/18 14:28:57 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/18 20:02:11 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "functions.h"
#include "sudoku.h"
#include "grid.h"

t_bool	solve_sudoku(int ***grid)
{
	int row;
	int col;
	int num;

	row = 0;
	col = 0;
	num = 1;
	if (!find_unassigned(*grid, &row, &col))
		return (1);
	while (num <= 9)
	{
		if (!check_conflicts(*grid, row, col, num))
		{
			(*grid)[row][col] = num;
			if (solve_sudoku(grid))
				return (1);
			(*grid)[row][col] = 0;
		}
		num++;
	}
	return (0);
}

t_bool	rev_solve_sudoku(int ***grid)
{
	int row;
	int col;
	int num;

	row = 0;
	col = 0;
	num = 9;
	if (!find_unassigned(*grid, &row, &col))
		return (1);
	while (num > 0)
	{
		if (!check_conflicts(*grid, row, col, num))
		{
			(*grid)[row][col] = num;
			if (solve_sudoku(grid))
				return (1);
			(*grid)[row][col] = 0;
		}
		num--;
	}
	return (0);
}

t_bool	grid_is_valid(int **grid)
{
	int num;
	int row;

	num = 1;
	while (num <= 9)
	{
		row = 0;
		while (row < 9)
		{
			if (!is_used_in_row(grid, row, num))
				return (0);
			row++;
		}
		num++;
	}
	return (1);
}

int		return_error(void)
{
	ft_putstr("Error\n");
	return (0);
}

int		main(int argc, char **argv)
{
	int **grid;
	int **rev_grid;

	if (argc != 10)
		return (return_error());
	argv++;
	grid = populate_array(argv);
	rev_grid = populate_array(argv);
	if (!grid)
		return (return_error());
	if (!solve_sudoku(&grid))
		return (return_error());
	if (!rev_solve_sudoku(&rev_grid))
		return (return_error());
	if (!grid_cmp(grid, rev_grid))
		return (return_error());
	if (!grid_is_valid(grid))
		return (return_error());
	print_grid(grid);
	return (0);
}
