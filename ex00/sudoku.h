/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   sudoku.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/18 12:14:39 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/18 13:39:37 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUDOKU_H
# define SUDOKU_H

# include "functions.h"

t_bool	is_used_in_row(int **grid, int row, int num);
t_bool	is_used_in_col(int **grid, int col, int num);
t_bool	is_used_in_box(int **grid, int s_row, int s_col, int num);
t_bool	check_conflicts(int **grid, int row, int col, int num);
t_bool	find_unassigned(int **grid, int *row, int *col);

#endif
