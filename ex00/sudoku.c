/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   sudoku.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/18 13:17:17 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/18 16:41:15 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "functions.h"

t_bool	is_used_in_row(int **grid, int row, int num)
{
	int	col;

	col = 0;
	while (col < 9)
	{
		if (grid[row][col] == num)
			return (1);
		col++;
	}
	return (0);
}

t_bool	is_used_in_col(int **grid, int col, int num)
{
	int row;

	row = 0;
	while (row < 9)
	{
		if (grid[row][col] == num)
			return (1);
		row++;
	}
	return (0);
}

t_bool	is_used_in_box(int **grid, int s_row, int s_col, int num)
{
	int row;
	int col;

	row = 0;
	col = 0;
	while (row < 3)
	{
		while (col < 3)
		{
			if (grid[row + s_row][col + s_col] == num)
				return (1);
			col++;
		}
		row++;
	}
	return (0);
}

t_bool	check_conflicts(int **grid, int row, int col, int num)
{
	if (is_used_in_row(grid, row, num))
		return (1);
	if (is_used_in_col(grid, col, num))
		return (1);
	if (is_used_in_box(grid, row - row % 3, col - col % 3, num))
		return (1);
	return (0);
}

t_bool	find_unassigned(int **grid, int *row, int *col)
{
	while (*row < 9)
	{
		*col = 0;
		while (*col < 9)
		{
			if (grid[*row][*col] == 0)
				return (1);
			(*col)++;
		}
		(*row)++;
	}
	return (0);
}
