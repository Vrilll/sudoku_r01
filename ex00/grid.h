/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   grid.h                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/18 14:31:46 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/18 19:57:11 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef GRID_H
# define GRID_H

int		*str_to_row(char *str);
int		**populate_array(char **str_arr);
void	print_grid(int **grid);
t_bool	grid_cmp(int **grid_a, int **grid_b);
#endif
