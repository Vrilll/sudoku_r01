/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   functions.h                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/18 12:12:27 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/18 14:37:19 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FUNCTIONS_H
# define FUNCTIONS_H

typedef	int	t_bool;
void	ft_putchar(char c);
void	ft_putstr(char *str);
int		ft_ctoi(char c);
int		ft_strlen(char *str);
#endif
