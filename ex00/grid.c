/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   grid.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/18 14:35:57 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/18 19:58:22 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "functions.h"

int		*str_to_row(char *str)
{
	int	*row;
	int	pos;
	int	num;

	if (ft_strlen(str) != 9)
		return (0);
	pos = 0;
	row = malloc(sizeof(int) * 9);
	while (pos < 9)
	{
		num = ft_ctoi(str[pos]);
		if (num == -1)
			return (0);
		row[pos] = num;
		pos++;
	}
	return (row);
}

int		**populate_array(char **str_arr)
{
	int	pos;
	int	**grid;

	pos = 0;
	grid = malloc(sizeof(int *) * 9);
	while (pos < 9)
	{
		grid[pos] = str_to_row(str_arr[pos]);
		if (!grid[pos])
			return (0);
		pos++;
	}
	return (grid);
}

void	print_grid(int **grid)
{
	int row;
	int col;

	row = 0;
	while (row < 9)
	{
		col = 0;
		while (col < 9)
		{
			ft_putchar(grid[row][col] + '0');
			col++;
			if (col != 9)
				ft_putchar(' ');
		}
		row++;
		ft_putchar('\n');
	}
}

t_bool	grid_cmp(int **grid_a, int **grid_b)
{
	int	row;
	int	col;

	row = 0;
	while (row < 9)
	{
		col = 0;
		while (col < 9)
		{
			if (grid_a[row][col] != grid_b[row][col])
				return (0);
			col++;
		}
		row++;
	}
	return (1);
}
